package aula18102019;

import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

// @TestInstance(LyfeCycle.PER.CLASS)

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class IntanceTest {

    private static double valor;


    // @BeforeAll // executa antes de um determinado codigo. Ex.: limpar banco de dados antes de popular o banco
    public IntanceTest() {

        valor = Math.random();
    }

    // gradle test

    @ParameterizedTest
    @ValueSource(ints={1, 2, 3, 4, 5})
    public void testCase01(int x) {

        // System.out.println(valor);

        Calc calc = new Calc();
        //calc.quadrado(10)


        assertEquals(x * x, calc.quadrado(x));
    }

    /*
    @Disabled
    @Test
    public void testCase02() {

        System.out.println(valor);
        assertEquals(1, 2);
    }

     */


}
